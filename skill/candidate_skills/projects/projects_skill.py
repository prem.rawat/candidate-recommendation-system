class ProjectSkill(object):
    def __init__(self, models, weight_per_project=0.2, threshold=0.5) -> None:
        """
        models: dict; contains object of skill extractor
        threshold: float; use for filtering low scoring skills
        weight_per_project: float; use for getting expertise score.
        Note: keys should be exactly same as below examples.
        e.g. models = {'skill_extractor_object': skill_extractor_model,}
        """
        self.skill_extractor = models
        self.threshold = threshold
        self.weight = weight_per_project
        self.skill_expertise_score = dict()

    def update_expertise_score(self, skills):
        """
        This function will update the expertise score of given skill based
        on number of projects.
        skills: list; list of skills extracted from projects
        """
        for skill in skills:
            if skill not in self.skill_expertise_score:
                self.skill_expertise_score[skill] = self.weight
            else:
                if (self.skill_expertise_score[skill] + self.weight) >= 1.0:
                    self.skill_expertise_score[skill] = 1.0
                else:
                    self.skill_expertise_score[skill] += self.weight

    def filter(self, skill_set):
        """
        skill_set: dict; it takes extracted skill with score
                        and filter skills based on score.

        return: filtered dict
        """
        return {k: v for k, v in skill_set.items() if v >= self.threshold}

    def extract_skill(self, project_info):
        """
        project_info: dict, title, description and tools & technology of the set of projects
        Note: keys should be exactly same as below examples
        e.g.  project_info = {1: {'TITLE': 'text title',
                                  'DESCRIPTION': 'text description',
                                  'TOOLS_TECHNOLOGY': 'list of tools and techs'
                                  },
                              2: {'TITLE: 'text title',
                                  'DESCRIPTION': 'text description',
                                  'TOOLS_TECHNOLOGY': 'list of tools and techs'
                                  } ...
                              }
        """
        self.skill_expertise_score.clear()
        for _, project_detail in project_info.items():
            skill_set = self.skill_extractor.extract_skill(project_detail['DESCRIPTION'])
            skill_set = self.filter(skill_set)
            skill_set = list(skill_set.keys())
            skill_set.extend([str(skill).title() for skill in project_detail['TOOLS_TECHNOLOGY']])
            skill_set = list(set(skill_set))
            self.update_expertise_score(skill_set)
        return self.skill_expertise_score
