import json


class PatentsModule(object):
    def __init__(self, config_file):
        """
        patent_status_scoring: dict, it contains scoring criteria for each level.
        threshold: float, it is upper bound of patent score

        keys of the dictionary must be same as below example.
        e.g.  patent_status_scoring = {'Issued': 1.0, 'Pending': 0.75}
        threshold = 8.0
        """
        with open(config_file, "r") as fp:
            params = json.load(fp)
        self.patent_status = params["PATENT_STATUS_WEIGH"]
        self.threshold = params["THRESHOLD"]

    def get_patents_score(self, patents_info):
        """
        patents_info: dict, it contains patents information of
        given candidate
        e.g. patents_info = {'1': {'NAME': 'abc', 'STATUS': 'Issued'},
                            '2': {'NAME': 'xyz', 'STATUS': 'Pending'}, ...
                           }
        """
        patents_score = 0.0
        for i, patent in patents_info.items():
            patents_score += self.patent_status[(patent['STATUS'].title())]
        if patents_score > self.threshold:
            agg_patent_score = 1.0
        else:
            agg_patent_score = patents_score / self.threshold
        return agg_patent_score
