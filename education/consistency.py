import json
import numpy as np


class ConsistencyModule(object):
    def __init__(self, config_file):
        """
        This Module will use for finding the consistency of candidate.
        criteria: string text
                  if "percentage" => module will find the consistency
                                      using percentage.
                  if "percentile" => module will convert the percentage to percentile
                                      and need year wise highest score corresponding
                                      to each board.

        weightage: dict, contain the factor values which to calculate
                        the final grit score
        e.g. weightage = {'DEGREE': 0.85, 'SUBJECTS': 0.15}
        """
        with open(config_file, "r") as f:
            params = json.load(f)
        self.criteria = params["CRITERIA"]
        self.averaging_criteria = params["AVG CRITERIA"]
        self.w = params["WEIGH"]
        self.null = params["NULL"]

    @staticmethod
    def collect_degree_percentage(edu_info):
        """
        This module will collect the all percentage marks
        of 10th, 12th and degree
        """
        degree_percentage = []

        info_10th = edu_info['SECONDARY/MATRICULATION']
        if 'PERCENTAGE' in info_10th:
            degree_percentage.append(info_10th['PERCENTAGE'])

        info_12th = edu_info['SENIOR SECONDARY']
        info_diploma = edu_info['DIPLOMA']
        if 'PERCENTAGE' in info_12th:
            degree_percentage.append(info_12th['PERCENTAGE'])
        elif 'PERCENTAGE' in info_diploma:
            degree_percentage.append(info_diploma['PERCENTAGE'])
        else:
            degree_percentage.append(33)

        info_degree = edu_info['GRADUATION']
        if 'PERCENTAGE' in info_degree:
            degree_percentage.append(info_degree['PERCENTAGE'])
        else:
            degree_percentage.append(33)
        return np.array(degree_percentage)

    @staticmethod
    def collect_subjects_mark(edu_info):
        """
        This module return the array of marks for top 3 subjects
        """
        top_subjects = edu_info['TOP SUBJECTS']
        if bool(top_subjects):
            marks = np.array(list(top_subjects.values()))
        else:
            marks = np.array([0, 0, 0])
        return marks

    def use_percentage(self, degree_percent, subject_marks):
        if self.averaging_criteria == 'mean':
            avg_degree_percent = np.average(degree_percent)
            avg_subject_marks = np.average(subject_marks)
        elif self.averaging_criteria == 'median':
            avg_degree_percent = np.median(degree_percent)
            avg_subject_marks = np.median(subject_marks)
        else:
            raise Exception("{} averaging criteria isn't exist.".format(self.averaging_criteria))

        std_degree_percent = np.std(degree_percent)
        std_subject_marks = np.std(subject_marks)

        degree_performance = self.w["SCHOOL/DEGREE"] * avg_degree_percent
        degree_variation = self.w["SCHOOL/DEGREE"] * std_degree_percent

        subject_performance = self.w["SUBJECTS"] * avg_subject_marks
        subjects_variation = self.w["SUBJECTS"] * std_subject_marks

        grit_score = (degree_performance + subject_performance) - (degree_variation + subjects_variation)
        return grit_score / 100.0

    def use_percentile(self, edu_info):
        pass

    def get_consistency_score(self, edu_info):
        """
        This module return the consistency score of candidate based on
        educational information.
        """
        degree_percent = self.collect_degree_percentage(edu_info)
        subject_percent = self.collect_subjects_mark(edu_info)
        score = 0.0
        if self.criteria == "percentage":
            score = self.use_percentage(degree_percent, subject_percent)
        elif self.criteria == "percentile":
            self.use_percentile(edu_info)
        else:
            raise Exception("Selected criteria isn't exist.")
        return score
