import json


class RanksModule(object):
    def __init__(self, config_file):
        """
        level_scoring: dict, it contains scoring criteria for each level.
        rank_scoring: dict, it contains scoring criteria for each rank
        weightage: dict, it contains weightage for level and rank
        threshold: float, it is upper bound of ranks score

        keys of the dictionary must be same as below example.
        e.g.  level_scoring = {'International': 1.0, 'National': 0.75,
                                'State/Regional': 0.4, 'University/College': 0.15
                              }
              rank_scoring = {'TOP 10': 1.0, '11-100': 0.85, '101-500': 0.65,
                              '501-1000': 0.55, 'NaN': 0.3}
              weightage = {'level_weight': 0.5, 'rank_weight': 0.5}
              threshold = 8.0
        """
        with open(config_file, "r") as fp:
            params = json.load(fp)
        self.level = params["LEVELS"]
        self.rank = params["RANK_SCORING"]
        self.weights = params["WEIGH"]
        self.threshold = params["THRESHOLD"]

    def get_ranks_score(self, ranks_info):
        """
        ranks_info: dict, it contains ranks information of
        given candidate
        e.g. ranks_info = {'1':{'LEVEL': 'National', 'OBTAINED RANK': 300, 'TITLE': 'JEE Mains'},
                           '2':{'LEVEL': 'International', 'OBTAINED RANK': 'NaN', 'TITLE': 'XYZ'},
                            ...
                          }
        """
        ranks_score = 0.0
        for i, rank in ranks_info.items():
            level_score = self.level[rank['LEVEL']]
            rank_score = self.rank[rank['OBTAINED RANK']]
            level_weighted_sum = (level_score * self.weights['LEVEL_WEIGH'])
            rank_weighted_sum = (rank_score * self.weights['RANK_WEIGH'])
            ranks_score += (level_weighted_sum + rank_weighted_sum)

        if ranks_score > self.threshold:
            agg_ranks_score = 1.0
        else:
            agg_ranks_score = ranks_score / self.threshold
        return agg_ranks_score
