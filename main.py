import json
import pandas as pd
from achievement import AchievementModule
from education import ConsistencyModule
from skill.candidate_skills import CandidateSkill
from skill.skill_extractor import FinalSkillExtractor
from skill import SkillMatcher
import time


def read_json_file(filename):
    with open(filename, "r") as f:
        data = json.load(f)
    return data


def read_jd(filename):
    with open(filename, "r") as f:
        data = f.read()
    return data


def recommendation_score(module_scores, module_weightage):
    """
  module_scores: dict; contains calculated score from modules like skill,
  achievement and educational consistency
  e.g. module_scores = {'Affinity Score': 0.43,
                        'Educational Consistency Score': 0.76,
                        'Achievement Score': 0.67}

  module_weightage: dict, contains weights corresponding to each module
  e.g. module_weightage = {'Skill Module': 0.7,
                           'Educational Module': 0.2,
                           'Achievements Module': 0.1}
  """
    weighted_affinity = module_scores['Affinity Score'] * module_weightage['Skill Module']
    weighted_consistency = module_scores['Educational Consistency Score'] * module_weightage['Educational Module']
    weighted_achievement = module_scores['Achievement Score'] * module_weightage['Achievements Module']

    final_score = weighted_affinity + weighted_consistency + weighted_achievement

    return final_score


def main(file_name="ranked_candidates.xlsx"):
    c30_data = {
        "ID": [],
        "JD_SKILLS": [],
        "CAND_SKILLS": [],
        "ACHIEVEMENT_SCORE": [],
        "GRIT_SCORE": [],
        "AFFINITY_SCORE": [],
        "FINAL_SCORE": []
    }

    candidates_data = read_json_file(filename="c30_data.json")
    jd_text = read_jd(filename="jd")
    fse = FinalSkillExtractor(config_file="skill/skill_extractor/integration_config.json")
    jd_skills = fse.extract_skill(jd_text)

    am = AchievementModule(config_file="achievement/achievements_config.json")
    cm = ConsistencyModule(config_file="education/education_config.json")

    sm = SkillMatcher(jd_skills=jd_skills, config_file="skill/skill_config.json")
    i = 0
    for candidate in candidates_data:
        cs = CandidateSkill(config_file="skill/candidate_skills/integrate_config.json")
        candidate_skills = cs.get_skills(candidate_info=candidate)
        affinity_score = sm.cal_affinity_score(cand_skills_exp_score=candidate_skills)

        achievement_score = am.get_achievement_score(candidate_info=candidate)
        grit_score = cm.get_consistency_score(edu_info=candidate["EDUCATIONAL INFO"])

        rec_score = recommendation_score(module_scores={
            'Affinity Score': affinity_score,
            'Educational Consistency Score': grit_score,
            'Achievement Score': achievement_score
        },
            module_weightage={
                'Skill Module': 0.9,
                'Educational Module': 0.07,
                'Achievements Module': 0.03
            }
        )

        c30_data["ID"].append(candidate["ID"])
        jd_skills = sorted(jd_skills)
        c30_data["JD_SKILLS"].append(",".join(jd_skills))
        cand_sk = sorted(list(candidate_skills.keys()))
        c30_data["CAND_SKILLS"].append(",".join(cand_sk))
        c30_data["ACHIEVEMENT_SCORE"].append(achievement_score)
        c30_data["GRIT_SCORE"].append(grit_score)
        c30_data["AFFINITY_SCORE"].append(affinity_score)
        c30_data["FINAL_SCORE"].append(rec_score)
        i += 1
        print("{}".format(i), end=", ")
    dataframe = pd.DataFrame(c30_data)
    dataframe.sort_values(by="FINAL_SCORE", inplace=True, ascending=False)
    dataframe["PREDICTED_RANK"] = list(range(1, dataframe.shape[0]+1))
    dataframe.to_excel(file_name, index=False)
    return dataframe


if __name__ == "__main__":
    start = time.time()
    use = "with_all"
    file = "ranked_candidates_{}.xlsx".format(use)
    print("Start Ranking...")
    data = main(file_name=file)
    print("Done.")
    print(data.head())
    end = time.time()
    print("DONE, CANDIDATES RANKED and SAVED into {}.".format(file))
    print("TIME TAKEN: {}.".format(end - start))
