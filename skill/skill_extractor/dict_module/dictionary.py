import os
import re
import json
import numpy as np
import pandas as pd


class SkillsDict(object):
    def __init__(self, filepath, colname="SKILLS") -> None:
        self.skills_set = np.unique(np.array(pd.read_csv(filepath,
                                                         encoding='unicode_escape',
                                                         usecols=[colname])[colname]))

    def extractskills(self, text):
        extracted_skill_set = list()
        for skill in self.skills_set:
            try:
                if len(re.findall(r'\b{}\b'.format(str(skill)), str(text), flags=re.IGNORECASE)) > 0:
                    extracted_skill_set.append(str(skill).title())
            except re.error:
                continue
        return set(extracted_skill_set)


class SkillsMatchedDicts(object):
    def __init__(self, parameters=None, config_file=None) -> None:
        if config_file:
            with open(config_file, "r") as file:
                params = json.load(file)
            self.folder = os.path.join(os.path.abspath(os.curdir), params["ROOT"])
            self.files = {f: w for f, w in params["WEIGH"].items() if params["SWITCH"][f]}
        elif parameters:
            self.folder = os.path.join(os.path.abspath(os.curdir), parameters["ROOT"])
            self.files = {f: w for f, w in parameters["FILES_WEIGH"].items() if parameters["SWITCH"][f]}
        else:
            raise Exception("Both parameters and config_file is None.")

    def get_skills_set(self, text):
        combined_skills_score = dict()
        for file_name, score in self.files.items():
            skills_set = SkillsDict(filepath=os.path.join(self.folder, file_name)).extractskills(text)
            for skill in skills_set:
                skill = skill.title()
                if skill not in combined_skills_score:
                    combined_skills_score[skill] = score
                else:
                    combined_skills_score[skill] += score
        return combined_skills_score


class DictModel(object):
    def __init__(self, parameters=None, config_file=None) -> None:
        """
        root: path to the dictionary folder
        dicts_scoring: dict, contain weightage for each dict file.
        iws: int, highest window size (for word tokens)
        """
        if config_file:
            with open(config_file, "r") as file:
                parameters = json.load(file)
        self.weightage = parameters["FILES_WEIGH"]
        self.initial_window_size = parameters["Initial WS"]
        self.skills_dicts = dict()
        for filename in parameters["FILES_WEIGH"]:
            with open(os.path.join(parameters["ROOT"], filename), "r") as fp:
                skills = [i.title() for i in fp.read().split("\n") if i.lower() not in ['', "skills"]]
                self.skills_dicts[filename] = skills

    def get_skills_set(self, text):
        """
    This function will return extracted skills from dictionaries.
    text: string text... here we can give jd_text, project description etc.
    Return: final extracted skills with commulative weighting points.
    """
        text = re.sub(r"[()\"#/@;:<>{}=~|.?,]", " ", text).lower()
        word_count = len(text.split())
        # Initialize Sliding Window Size
        if word_count >= self.initial_window_size:
            i = self.initial_window_size
        else:
            i = word_count

        final_skills = dict()
        while i:
            word_tokens = text.split()
            extracted_skills = dict()
            for j in range(len(word_tokens) - i + 1):
                word = (" ".join(word_tokens[j:j + i])).title()
                for k in self.skills_dicts:
                    if (word in self.skills_dicts[k]) and (word not in extracted_skills):
                        extracted_skills[word] = self.weightage[k]
                    elif (word in self.skills_dicts[k]) and (word in extracted_skills):
                        extracted_skills[word] += self.weightage[k]
                    else:
                        continue

            for skill in extracted_skills:
                text.replace(skill, "")
            i -= 1
            final_skills.update(extracted_skills)
        return final_skills
