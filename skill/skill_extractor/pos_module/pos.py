import re
import string


def has_numbers(text):
    return any(char.isdigit() for char in text)


def check_splcharacter(text):
    string_check = re.compile('[_#]')
    return string_check.search(text)


def remove_urls(text):
    text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', text, flags=re.MULTILINE)
    return text


def remove_org_loc(text, nlp_model):
    document = nlp_model(text)
    location_org = [str(ent.text) for ent in document.ents if ((ent.label_ == 'ORG') or (ent.label_ == 'GPE'))]
    for lo in location_org:
        try:
            text = re.sub(r'\b{}\b'.format(str(lo)), '', text)
        except re.error:
            continue
    text = text.translate(str.maketrans('', '', string.punctuation))
    return text.strip()


def update_dict(dictionary, element, score=1.0):
    if element not in dictionary:
        dictionary[element] = score
    else:
        dictionary[element] = dictionary[element] + score


# rules for skill extraction
def skill_extraction(text, nlp_model):
    # remove_url
    text = remove_urls(text)
    # remove locations and organisation
    text = remove_org_loc(text, nlp_model=nlp_model)

    document = nlp_model(text)

    skill_set = {}

    try:
        previous_token = document[0]
        nxt = document[1]
    except IndexError:
        return skill_set

    # Token and Tag
    for token in document[2:]:
        if (previous_token.pos_ == 'PROPN') and (nxt.pos_ == 'PROPN' or nxt.pos_ == 'VERB'):
            candidate_skill = ' '.join([str(previous_token), str(nxt)])
            update_dict(skill_set, candidate_skill, score=0.25)

        if (previous_token.pos_ == 'PROPN') and str(nxt) == '/' and (token.pos_ == 'PROPN'):
            candidate_skill = str(token)
            update_dict(skill_set, candidate_skill, score=0.5)

            candidate_skill = str(previous_token)
            update_dict(skill_set, candidate_skill, score=0.6)

        if (str(previous_token)[len(str(previous_token)) - 1] == '/') and (nxt.pos_ == 'PROPN'):
            candidate_skill = str(nxt)
            update_dict(skill_set, candidate_skill, score=0.45)

            candidate_skill = str(previous_token)[:-1]
            update_dict(skill_set, candidate_skill, score=0.48)

        if (previous_token.pos_ == 'PROPN') and (nxt.pos_ == 'CCONJ') and (token.pos_ == 'PROPN'):
            candidate_skill = str(token)
            update_dict(skill_set, candidate_skill, score=0.68)

            candidate_skill = str(previous_token)
            update_dict(skill_set, candidate_skill, score=0.62)

        if (previous_token.pos_ == 'PROPN') and str(nxt) == ',' and (token.pos_ == 'PROPN'):
            candidate_skill = str(token)
            update_dict(skill_set, candidate_skill, score=0.54)

            candidate_skill = str(previous_token)
            update_dict(skill_set, candidate_skill, score=0.38)

        if str(nxt)[0].isupper() and nxt.pos_ == 'PROPN':
            candidate_skill = str(nxt)
            update_dict(skill_set, candidate_skill, score=0.3)

        if has_numbers(str(nxt)):
            candidate_skill = str(nxt)
            update_dict(skill_set, candidate_skill, score=0.34)

        if check_splcharacter(str(nxt)):
            candidate_skill = str(nxt)
            update_dict(skill_set, candidate_skill, score=0.48)

        if (nxt.pos_ == 'ADP') and (token.pos_ == 'NOUN' or token.pos_ == 'PROPN'):
            candidate_skill = str(token)
            update_dict(skill_set, candidate_skill, score=0.6)

        previous_token = nxt
        nxt = token

    garbage = [k for k, v in skill_set.items() if k.isdecimal()]

    skill_set = dict([(skill.title(), score) for skill, score in skill_set.items() if skill not in garbage])

    return skill_set
