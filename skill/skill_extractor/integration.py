import json
import spacy
from skill.skill_extractor.dict_module import SkillsMatchedDicts, DictModel
from skill.skill_extractor.ner_module import NERModel
from skill.skill_extractor.pos_module import skill_extraction


class FinalSkillExtractor(object):
    def __init__(self, parameters=None, config_file=None):
        if config_file:
            with open(config_file, "r") as file:
                params = json.load(file)
        elif parameters:
            params = parameters
        else:
            raise Exception("Both parameters and config_file is None.")
        self.switch = params["SWITCH"]
        self.dict_weightage = params["DICT"]["WEIGHTAGE"]
        self.ner_weightage = params["NER"]["WEIGHTAGE"]
        self.pos_weightage = params["POS"]["WEIGHTAGE"]
        self.dictionary_module = DictModel(parameters=params["DICT"])
        self.ner_module = NERModel(parameters=params["NER"])
        self.nlp_model = spacy.load(params["POS"]["MODEL_NAME"])
        self.final_skill_set = {}

    def update_skill_score(self, skill, score):
        if skill not in self.final_skill_set:
            self.final_skill_set[skill] = score
        else:
            self.final_skill_set[skill] = self.final_skill_set[skill] + score

    def extract_skill(self, text):
        self.final_skill_set.clear()
        combined_skills = []
        dict_skills, ner_skills, pos_skills = {}, {}, {}
        if self.switch["DICT"]:
            dict_skills = self.dictionary_module.get_skills_set(text)  # dict skill set
            combined_skills.extend(list(dict_skills.keys()))
        if self.switch["NER"]:
            ner_skills = self.ner_module.get_skills_set(text)  # ner skill set
            combined_skills.extend(list(ner_skills.keys()))
        if self.switch["POS"]:
            pos_skills = skill_extraction(text, nlp_model=self.nlp_model)  # pos skill set
            combined_skills.extend(list(pos_skills.keys()))

        # print(combined_skills)
        combined_skills = set(combined_skills)
        for skill in combined_skills:
            if skill in dict_skills:
                self.update_skill_score(skill, dict_skills[skill] * self.dict_weightage)
            if skill in ner_skills:
                self.update_skill_score(skill, ner_skills[skill] * self.ner_weightage)
            if skill in pos_skills:
                self.update_skill_score(skill, pos_skills[skill] * self.pos_weightage)
        return self.final_skill_set
