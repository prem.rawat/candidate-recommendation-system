import json


class TestScoresModule(object):
    def __init__(self, config_file):
        """
        threshold: float, upper bound of scores in
        competitive exams of candidate
        """
        with open(config_file, "r") as file:
            params = json.load(file)
        self.threshold = params["THRESHOLD"]

    def get_percentage_sum(self, scores_info):
        """
        scores_info: dict, it contains score information of
        given candidate
        e.g. scores_info = {'1': {'TITLE': 'JEE Mains', 'OBTAINED SCORE': 290, 'TOTAL SCORE': 320},
                            '2': {'TITLE': 'JEE Advanced', 'OBTAINED SCORE': 250, 'TOTAL SCORE': 366},
                                  ...
                          }
        """
        percentage_sum_score = 0.0
        for i, score in scores_info.items():
            try:
                if (score["OBTAINED SCORE"] != "NaN") and (score["TOTAL SCORE"] != "NaN"):
                    percentage_sum_score += score['OBTAINED SCORE'] / score['TOTAL SCORE']
                else:
                    percentage_sum_score += 0.5
            except ZeroDivisionError:
                percentage_sum_score += 0.5
        if percentage_sum_score >= self.threshold:
            agg_percent_score = 1.0
        else:
            agg_percent_score = percentage_sum_score / self.threshold
        return agg_percent_score
