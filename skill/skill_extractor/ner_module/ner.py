import json
import spacy


class NERModel(object):
    def __init__(self, parameters=None, config_file=None):
        if config_file:
            with open(config_file, "r") as f:
                params = json.load(f)
        elif parameters:
            params = parameters
        else:
            raise Exception("Both parameters and config_file is None.")
        self.model = spacy.load(params["MODEL_NAME"])
        self.score = params["WEIGH"]

    def get_skills_set(self, text):
        document = self.model(text)
        skills = set([str(s) for s in document.ents])
        skill_with_score = dict()
        for skill in skills:
            skill = skill.translate(str.maketrans('', '', "!$%&'()*,.:;<=>?@[\]^_`{|}~)"))
            skill = skill.strip()
            if '/' in skill:
                sub_skills = skill.split('/')
                for ss in sub_skills:
                    ss = ss.title()
                    if ss not in skill_with_score:
                        skill_with_score[ss] = self.score
            else:
                skill = skill.title()
                if skill not in skill_with_score:
                    skill_with_score[skill] = self.score
        return skill_with_score
