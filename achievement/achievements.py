import json
from achievement.awards import AwardsModule
from achievement.patents import PatentsModule
from achievement.publications import PublicationsModule
from achievement.ranks import RanksModule
from achievement.tests import TestScoresModule


class AchievementModule(object):
    def __init__(self, config_file):
        """
        module_obj: dict, it contains all module's object of achievement
        weightage: dict, it contains weightage for level and rank

        keys of the dictionary must be same as below example.
        e.g.
              module_obj = {'awards_module': am_obj,
                            'patents_module': ptm_obj,
                            'publications_module': pm_obj,
                            'score_module': sm_obj,
                            'rank_module': rm_obj
                            }
              weightage = { 'awards': 0.2,
                            'patents': 0.2,
                            'publications': 0.2,
                            'scores': 0.2,
                            'ranks': 0.2
                          }
        """
        with open(config_file, "r") as file:
            params = json.load(file)
        self.modules_obj = {'awards_module': AwardsModule(config_file=params["CONFIGS"]["awards"]),
                            'patents_module': PatentsModule(config_file=params["CONFIGS"]["patents"]),
                            'publications_module': PublicationsModule(config_file=params["CONFIGS"]["publications"]),
                            'score_module': TestScoresModule(config_file=params["CONFIGS"]["test_score"]),
                            'rank_module': RanksModule(config_file=params["CONFIGS"]["rank"])
                            }
        self.weights = params["WEIGH"]

    def get_achievement_score(self, candidate_info):
        """
        candidate_info: dict, it contains candidate information of
        given candidate with achievements

        following key information must have for each candidate...
        e.g. candidate_info = {'AWARDS INFO':{'1': {'LEVEL': 'International', 'POSITION': 2, 'TITLE': 'XYZ'},
                                              '2': {'LEVEL': 'National', 'POSITION': 1, 'TITLE': 'ABC'},
                                              ...
                                              },
                               'PATENTS INFO':{'1': {'NAME': 'abc', 'STATUS': 'Issued'},
                                               '2': {'NAME': 'xyz', 'STATUS': 'Pending'},
                                               ...
                                               },
                               'PUBLICATIONS INFO': {'1': {'NAME': 'qwe', 'TYPE': 'Journal Paper'},
                                                     '2': {'NAME': 'xyz', 'TYPE': 'Conference Paper'},
                                                     ...
                                                     },
                               'RANK INFO': {'1': {'LEVEL': 'International', 'OBTAINED RANK': 'TOP 10',
                                                    'TITLE': 'JEE Mains'},
                                             '2': {'LEVEL': 'International', 'OBTAINED RANK': 'TOP 10', 'TITLE': 'XYZ'},
                                             ...
                                            },
                               'SCORE INFO': {'1': {'OBTAINED SCORE': 290, 'TITLE': 'JEE Mains', 'TOTAL SCORE': 320},
                                              '2': {'OBTAINED SCORE': 250, 'TITLE': 'JEE Advanced', 'TOTAL SCORE': 366},
                                              ...
                                              }
                             }
        """
        # weighted awards points of candidate
        awdp = self.modules_obj['awards_module'].get_awards_score(candidate_info['AWARDS INFO'])
        wawdp = awdp * self.weights['awards']

        # weighted patents points of candidate
        patp = self.modules_obj['patents_module'].get_patents_score(candidate_info['PATENTS INFO'])
        wpatp = patp * self.weights['patents']

        # weighted publications points of candidate
        pubp = self.modules_obj['publications_module'].get_publications_score(candidate_info['PUBLICATIONS INFO'])
        wpubp = pubp * self.weights['publications']

        # weighted competitive score points of candidate
        scrp = self.modules_obj['score_module'].get_percentage_sum(candidate_info['SCORE INFO'])
        wscrp = scrp * self.weights['test_score']

        # weighted ranking points of candidate
        rnkp = self.modules_obj['rank_module'].get_ranks_score(candidate_info['RANK INFO'])
        wrnkp = rnkp * self.weights['ranks']

        # accumulate final achievement score
        achievement_score = wawdp + wpatp + wpubp + wscrp + wrnkp

        return achievement_score
