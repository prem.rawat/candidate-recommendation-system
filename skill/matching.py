import json
import os
import numpy as np
from gensim.models import KeyedVectors


class Word2Vec(object):
    def __init__(self, wordvectorfile):
        if os.path.exists(wordvectorfile):
            self.wordfilepath = wordvectorfile
            self.vectors = KeyedVectors.load(self.wordfilepath, mmap='r')
        else:
            raise "{} is not exist!".format(wordvectorfile)

    @staticmethod
    def get_similarity_score(v1, v2):
        score = 0.0
        if v1.all() and v2.all():
            score = np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))
        return score

    def get_embedding(self, query):
        """
        This function will find the embedding of given query word.
        if a query word length is more than 1 word then it will take average
        of individual word embeddings.
        """
        i = 0
        token_vector = np.zeros(shape=(self.vectors.vector_size,))
        for token in query.split():
            i += 1
            try:
                token_vector += self.vectors[token]
            except KeyError:
                continue
        if i > 1:
            token_vector /= i
        return token_vector

    def get_top_similarity_score(self, query, jd_skills):
        query_embedding = self.get_embedding(str(query).lower())
        topmost_score = 0.0
        for skill in jd_skills:
            skill_embedding = self.get_embedding(str(skill).lower())
            similarity_score = self.get_similarity_score(query_embedding,
                                                         skill_embedding)
            if topmost_score < similarity_score:
                topmost_score = similarity_score
        return topmost_score


def exact_skill_match(candidate_skills, jd_skills):
    cand_set = candidate_skills.copy()
    jd_set = jd_skills.copy()
    exact_skills_matched = dict()
    for skill in candidate_skills:
        if skill in jd_skills:
            exact_skills_matched[skill] = 1.0
            cand_set.remove(skill)
            jd_set.remove(skill)
    return exact_skills_matched, cand_set, jd_set


class SkillMatcher(object):
    def __init__(self, jd_skills, config_file):
        with open(config_file, "r") as file:
            params = json.load(file)
        self.w2v = Word2Vec(wordvectorfile=params["WORD2VECTOR"])
        self.jd_skills = [i.strip() for i in jd_skills]
        self.threshold = params["THRESHOLD"]
        self.skill_with_sim_score = dict()
        self.skill_not_matched = list()

    def candidate_skill_similarity_score(self, candidate_skills):
        self.skill_with_sim_score.clear()
        exact_skills, rem_cand_skills, rem_jd_skills = exact_skill_match(candidate_skills,
                                                                         self.jd_skills)
        self.skill_with_sim_score.update(exact_skills)
        self.skill_not_matched.clear()
        for skill in rem_cand_skills:
            sim_score = self.w2v.get_top_similarity_score(skill, rem_jd_skills)
            if sim_score > self.threshold:
                self.skill_with_sim_score[skill] = sim_score
            else:
                self.skill_not_matched.append(skill)

    def cal_affinity_score(self, cand_skills_exp_score):
        cand_skill_set = set(cand_skills_exp_score.keys())
        self.candidate_skill_similarity_score(cand_skill_set)
        i, exp_score = 0, 0.0
        for skill in cand_skills_exp_score:
            if skill in self.skill_with_sim_score:
                exp_score += cand_skills_exp_score[skill] * self.skill_with_sim_score[skill]
                i += 1
        if i > 1:
            exp_score /= i
        affinity_score = exp_score * (len(self.skill_with_sim_score) / len(self.jd_skills))
        """try:
            affinity_score *= (1/(0.05*len(self.skill_not_matched)))
        except ZeroDivisionError:
            affinity_score *= 1"""
        return affinity_score
