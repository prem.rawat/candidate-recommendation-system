import json


class AwardsModule(object):
    def __init__(self, config_file, threshold=5.0):
        """
        config_file contain the information of below parameters
        level_scoring: dict, it contains scoring criteria for each level.
        rank_scoring: dict, it contains scoring criteria for each rank
        weightage: dict, it contains weightage for level and rank
        threshold: float, it is upper bound of awards score

        keys of the dictionary must be same as below example.
        e.g.  level_scoring = {'International': 1.0, 'National': 0.75,
                                'State/Regional': 0.4, 'University/College': 0.15
                              }
              rank_scoring = {"1": 1.0, "2": 0.85, "3": 0.65, 'NaN': 0.5}
              weightage = {'level_weight': 0.5, 'rank_weight': 0.5}
              threshold = 8.0
        """
        with open(config_file, "r") as fp:
            params = json.load(fp)
        self.level = params['LEVELS']
        self.rank = params['RANK_SCORING']
        self.weights = params['WEIGH']
        self.threshold = threshold

    def get_awards_score(self, awards_info):
        """
    awards_info: dict, it contains awards information of
    given candidate
    e.g. awards_info = {'1': {'LEVEL': 'International', 'POSITION': 2, 'TITLE': 'XYZ'},
                        '2': {'LEVEL': 'National', 'POSITION': 1, 'TITLE': 'ABC'}, ...
                       }
    """
        awards_score = 0.0
        for i, award in awards_info.items():
            level_sum_score = self.level[award['LEVEL']]
            rank_sum_score = self.rank[str(award['POSITION'])]
            level_weighted_sum = (level_sum_score * self.weights['LEVEL_WEIGH'])
            rank_weighted_sum = (rank_sum_score * self.weights['RANK_WEIGH'])
            awards_score += level_weighted_sum + rank_weighted_sum

        if awards_score > self.threshold:
            agg_award_score = 1.0
        else:
            agg_award_score = awards_score / self.threshold
        return agg_award_score
