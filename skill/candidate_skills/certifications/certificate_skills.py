class TrainingCertificationSkill(object):
    def __init__(self, models, weight_per_tc=0.25, threshold=0.5) -> None:
        """
        models: dict; contains object of skill extractor
        weight_per_tc: float; weight for skill on each certificate
        threshold: float; use for filtering low scoring skills
        Note: keys should be exactly same as below examples.
        e.g. models = {'skill_extractor_object': skill_extractor_model,}
        """
        self.skill_extractor = models
        self.threshold = threshold
        self.weight = weight_per_tc
        self.skill_expertise_score = dict()

    def update_expertise_score(self, skills):
        """
        This function will update the expertise score of given skill based
        on number of related trainings/certifications.
        skills: list; list of skills extracted from trainings/certifications
        """
        for skill in skills:
            if skill not in self.skill_expertise_score:
                self.skill_expertise_score[skill] = self.weight
            else:
                if (self.skill_expertise_score[skill] + self.weight) >= 1.0:
                    self.skill_expertise_score[skill] = 1.0
                else:
                    self.skill_expertise_score[skill] += self.weight

    def filter(self, skill_set):
        """
        skill_set: dict; it takes extracted skill with score
                        and filter skills based on score.

        return: filtered dict
        """
        return {k: v for k, v in skill_set.items() if v >= self.threshold}

    def extract_skill(self, tc_info):
        """
        tc_info: dict; title, description, tools_tech learned from training/certification
        Note: keys should be exactly same as below examples
        e.g.  tc_info = {1: {'TITLE': 'text title of training/certificate',
                             'DESCRIPTION': 'text description/summary',
                             'TOOLS_TECHNOLOGY': list of tools and tech. learned
                             },
                        2: {'TITLE': 'text title of training/certificate',
                            'DESCRIPTION': 'text description/summary',
                            'TOOLS_TECHNOLOGY': list of tools and tech. learned
                           } ...
                      }
        """
        self.skill_expertise_score.clear()
        for _, tc_detail in tc_info.items():
            title_desc_text = "{} {}".format(tc_detail['TITLE'], tc_detail['DESCRIPTION'])
            skill_set = self.skill_extractor.extract_skill(title_desc_text)
            skill_set = self.filter(skill_set)
            skill_set = list(skill_set.keys())
            skill_set.extend([str(skill).title() for skill in tc_detail['TOOLS_TECHNOLOGY']])
            skill_set = list(set(skill_set))
            self.update_expertise_score(skill_set)
        return self.skill_expertise_score
