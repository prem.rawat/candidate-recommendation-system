class ResearchPaperSkill(object):
    def __init__(self, models, weight_per_research=0.25, threshold=0.5) -> None:
        """
    models: object; contains object of skill extractor
    threshold: float; use for filtering low scoring skills
    Note: keys should be exactly same as below examples.
    e.g. models = skill_extractor_model
    """
        self.skill_extractor = models
        self.threshold = threshold
        self.weight = weight_per_research
        self.skill_expertise_score = dict()

    def update_expertise_score(self, skills):
        """
    This function will update the expertise score of given skill based
    on number of projects.
    skills: list; list of skills extracted from projects
    """
        for skill in skills:
            if skill not in self.skill_expertise_score:
                self.skill_expertise_score[skill] = self.weight
            else:
                if (self.skill_expertise_score[skill] + self.weight) >= 1.0:
                    self.skill_expertise_score[skill] = 1.0
                else:
                    self.skill_expertise_score[skill] += self.weight

    def filter(self, skill_set):
        """
    skill_set: dict; it takes extracted skill with score
                    and filter skills based on score.

    return: filtered dict
    """
        return {k: v for k, v in skill_set.items() if v >= self.threshold}

    def extract_skill(self, research_info):
        """
    research_info: dict; title, description/abstract of the research papers
    Note: keys should be exactly same as below examples
    e.g.  research_info = {1: {'TITLE': 'text title',
                               'DESCRIPTION': 'text description/abstract/summary',
                              },
                           2: {'TITLE': 'text title',
                               'DESCRIPTION': 'text description/abstract/summary',
                               } ...
                          }
    """
        self.skill_expertise_score.clear()
        for _, research_detail in research_info.items():
            title_desc_text = "{} {}".format(research_detail['TITLE'], research_detail['DESCRIPTION'])
            skill_set = self.skill_extractor.extract_skill(title_desc_text)
            skill_set = self.filter(skill_set)
            skill_set = list(set(skill_set.keys()))
            self.update_expertise_score(skill_set)
        return self.skill_expertise_score
