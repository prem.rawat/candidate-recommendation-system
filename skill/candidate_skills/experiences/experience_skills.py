class WorkExperienceSkill(object):
    def __init__(self, models, threshold=0.5) -> None:
        """
        models: dict; contains object of skill extractor
        threshold: float; use for filtering low scoring skills
        Note: keys should be exactly same as below examples.
        e.g. models = {'skill_extractor_object': skill_extractor_model,}
        """
        self.skill_extractor = models
        self.threshold = threshold
        self.full_expertise = {'Internship': 12,
                               'Freelance': 18,
                               'Job': 6,
                               'Part-time': 24,
                               'Full-time': 6,
                               'Full time': 6,
                               'Self-employed': 30,
                               'Other': 48
                               }
        self.skill_expertise_score = dict()
        self.weight = 0.0

    def get_weight(self):
        return self.weight

    def set_weight(self, weight):
        if weight >= 1.0:
            self.weight = 1.0
        else:
            self.weight = weight

    def update_expertise_score(self, skills):
        """
        This function will update the expertise score of given skill based
        on number of projects.
        skills: list; list of skills extracted from projects
        """
        for skill in skills:
            if skill not in self.skill_expertise_score:
                self.skill_expertise_score[skill] = self.weight
            else:
                if (self.skill_expertise_score[skill] + self.weight) > 1.0:
                    self.skill_expertise_score[skill] = 1.0
                else:
                    self.skill_expertise_score[skill] += self.weight

    def filter(self, skill_set):
        """
        skill_set: dict; it takes extracted skill with score
                        and filter skills based on score.

        return: filtered dict
        """
        return {k: v for k, v in skill_set.items() if v >= self.threshold}

    def extract_skill(self, work_exp_info):
        """
        work_exp_info: dict; title, description/summary of the work experience
        Note: keys should be exactly same as below example.
        e.g.  work_exp_info = {1: {'TYPE': 'type of work (internship/job/freelancing)',
                                   'DESCRIPTION': 'text description of work',
                                   'MONTH': int (number of months worked on)
                                  },
                               2: {'TITLE': 'text title',
                                   'DESCRIPTION': 'text description/abstract/summary',
                                   } ...
                              }
        """
        self.skill_expertise_score.clear()
        for _, work_detail in work_exp_info.items():
            skill_set = self.skill_extractor.extract_skill(work_detail['DESCRIPTION'])
            skill_set = self.filter(skill_set)
            self.set_weight((work_detail['MONTHS'] / self.full_expertise[work_detail['TYPE']]))
            self.update_expertise_score(skill_set)
        return self.skill_expertise_score
