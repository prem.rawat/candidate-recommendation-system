import json
from skill.skill_extractor import FinalSkillExtractor
from skill.candidate_skills.projects import ProjectSkill
from skill.candidate_skills.research import ResearchPaperSkill
from skill.candidate_skills.experiences import WorkExperienceSkill
from skill.candidate_skills.certifications import TrainingCertificationSkill


class CandidateSkill(object):
    def __init__(self, config_file):
        """
        models: dict; contains object of project, research, work experience
        and training/certification class respectively.
        Note: keys should be exactly same as below examples.
        e.g. models = {'PROJECT_OBJ': project_obj,
                       'RESEARCH_OBJ': research_obj,
                       'WORK_EXPERIENCE_OBJ': work_exp_obj,
                       'TRAINING_CERTIFICATION_OBJ': train_cert_obj
                      }

        final_scale_factor: dict(); contain scale factor for each module
        Note: keys should be exactly same as below examples.
        e.g., final_scale_factor = {'PROJECTS SKILL': 0.4,
                                    'RESEARCH SKILL': 0.15,
                                    'WORK_EXP SKILL': 0.1,
                                    'TRAIN_CERTIFICATION SKILL': 0.5
                                   }
        """
        with open(config_file, "r") as file:
            params = json.load(file)
        self.skill_extract_obj = FinalSkillExtractor(parameters=params["SKILL_EXTRACTOR"])
        self.project = ProjectSkill(models=self.skill_extract_obj)
        self.research = ResearchPaperSkill(models=self.skill_extract_obj)
        self.work_exp = WorkExperienceSkill(models=self.skill_extract_obj)
        self.train_cert = TrainingCertificationSkill(models=self.skill_extract_obj)
        self.scale_factors = params["FINAL_SCALE_FACTOR"]
        self.cand_skills = dict()

    def scaling_skill_score(self, dictionary):
        """
        This function scale the expertise score into range (0, 1]

        dictionary: dict; contain skills extracted from each module (project,
        research, work experience, training/certifications)
        e.g. dictionary = {'PROJECTS SKILL': {skill_1: score,
                                              skill_2: score, ... },
                           'RESEARCH_OBJ': {skill_1: score,
                                            skill_2: score, ... },
                           'WORK_EXPERIENCE SKILL': {skill_1: score,
                                                     skill_2: score, ... },
                           'TRAIN_CERTIFICATION SKILL': {skill_1: score,
                                                         skill_2: score, ... }
                          }
        """
        self.cand_skills.clear()
        for module, skill_set in dictionary.items():
            for skill, score in skill_set.items():
                if skill not in self.cand_skills:
                    self.cand_skills[skill] = (score * self.scale_factors[module])
                else:
                    self.cand_skills[skill] += (score * self.scale_factors[module])

    def get_skills(self, candidate_info):
        """
        This function return skills extracted from candidate profile and
        it is taking project, research/publication, work experience and
        training/certifications factors into consideration.

        candidate_info: dict; consist of candidate information in dictionary form.

        e.g. candidate_info = {'ID': int/string,
                               'PROJECT INFO': {1: {}, 2: {}, ...},
                               'RESEARCH INFO': {1: {}, 2: {}, ...},
                               'WORK EXPERIENCE INFO': {1: {}, 2: {}, ...},
                               'TRAINING & CERTIFICATION INFO'': {1: {}, 2: {}, ...}
                              }
        """
        final_skill_set = dict()
        final_skill_set['PROJECTS SKILL'] = self.project.extract_skill(candidate_info['PROJECTS INFO'])
        final_skill_set['RESEARCH SKILL'] = self.research.extract_skill(candidate_info['RESEARCH INFO'])
        final_skill_set['WORK_EXPERIENCE SKILL'] = self.work_exp.extract_skill(candidate_info['WORK EXPERIENCE INFO'])
        final_skill_set['TRAIN_CERTIFICATION SKILL'] = self.train_cert.extract_skill(
            candidate_info['TRAINING & CERTIFICATION INFO'])

        self.scaling_skill_score(final_skill_set)
        return self.cand_skills
