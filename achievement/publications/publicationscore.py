import json


class PublicationsModule(object):
    def __init__(self, config_file):
        """
        publication_scoring: dict, it contains scoring criteria
                                  for each type of publication.

        keys of the dictionary must be same as below example.
        e.g.  publication_scoring = {'Journal Paper': 1.0, 'Book': 0.80,
                                    'Conference Paper': 0.75, 'Chapter': 0.65,
                                    'others':0.55
                                    }
        """
        with open(config_file, "r") as fp:
            params = json.load(fp)
        self.publication = params["PUBLICATION TYPE SCORING"]
        self.threshold = params["THRESHOLD"]

    def get_publications_score(self, publication_info):
        """
        publication_info: dict, it contains publication information of
        given candidate
        e.g. publication_info = {'1': {'NAME': 'qwe', 'TYPE': 'Journal Paper'},
                                '2': {'NAME': 'xyz', 'TYPE': 'Conference Paper'},
                                ...
                                }
        """
        publication_sum_score = 0.0
        for i, publication in publication_info.items():
            publication_sum_score += self.publication[publication['TYPE']]

        if publication_sum_score >= self.threshold:
            agg_pub_score = 1.0
        else:
            agg_pub_score = publication_sum_score / self.threshold
        return agg_pub_score
